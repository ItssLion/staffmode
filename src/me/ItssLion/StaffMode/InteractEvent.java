package me.ItssLion.StaffMode;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class InteractEvent implements Listener {

    StaffMode main;

    InteractEvent(StaffMode main) {
        this.main = main;
    }
    @EventHandler
    public void onInteract(PlayerInteractEvent e) {
        if (main.toggled.contains(e.getPlayer().getUniqueId())) {
            switch (e.getMaterial()) {
                case INK_SACK:
                    ItemStack is = e.getPlayer().getItemInHand();
                    if (is.getDurability() == 1) {
                        e.getPlayer().chat("/vanish");
                        ItemStack stack = new ItemStack(Material.INK_SACK, 1, (short) 10);
                        ItemMeta stackm = stack.getItemMeta();
                        stackm.setDisplayName(Utils.format("&c&lDisable Vanish"));
                        stack.setItemMeta(stackm);
                        e.getPlayer().getInventory().setItem(4, stack);
                    } else {
                        e.getPlayer().chat("/vanish");
                        ItemStack vanish = new ItemStack(Material.INK_SACK, 1, (short) 1);
                        ItemMeta vanishm = vanish.getItemMeta();
                        vanishm.setDisplayName(Utils.format("&a&lEnable Vanish"));
                        vanish.setItemMeta(vanishm);
                        e.getPlayer().getInventory().setItem(4, vanish);
                    }
                default:
                    break;
            }
            e.setCancelled(true);
        }
    }
}
