package me.ItssLion.StaffMode;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;

public class Utils {

    public static String format(String str) { return ChatColor.translateAlternateColorCodes('&', str); }

    public static void giveItems(Player p) {
        PlayerInventory inv = p.getInventory();
        ItemStack vanish = new ItemStack(Material.INK_SACK, 1, (short) 1);
        ItemMeta vanishm = vanish.getItemMeta();
        vanishm.setDisplayName(format("&a&lEnable Vanish"));
        vanish.setItemMeta(vanishm);
        inv.setItem(4, vanish);
    }
}
