package me.ItssLion.StaffMode;

import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

public class StaffMode extends JavaPlugin {
    public void onEnable() {
        getCommand("staffmode").setExecutor(new StaffModeCommand(this));
        getServer().getPluginManager().registerEvents(new DropEvent(this), this);
        getServer().getPluginManager().registerEvents(new InteractEvent(this), this);
        getServer().getPluginManager().registerEvents(new ClickEvent(this), this);
        new InventoryManager(this);
    }

    String prefix = "&8[&c&lOverhaul&4&lStaff&8] ";
    ArrayList<UUID> toggled = new ArrayList<>();
    HashMap<UUID, ItemStack[]> armor = new HashMap<>();
    HashMap<UUID, ItemStack[]> inventory = new HashMap<>();
}
