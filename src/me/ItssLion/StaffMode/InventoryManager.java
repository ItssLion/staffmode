package me.ItssLion.StaffMode;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class InventoryManager {

    private static StaffMode main;

    InventoryManager(StaffMode main) {
        InventoryManager.main = main;
    }

    public static void saveInventory(Player p) {
        main.armor.put(p.getUniqueId(), p.getInventory().getArmorContents());
        main.inventory.put(p.getUniqueId(), p.getInventory().getContents());
    }

    public static void restoreInventory(Player p) {
        ItemStack[] content = main.armor.get(p.getUniqueId());
        p.getInventory().setArmorContents(content);
        content = main.inventory.get(p.getUniqueId());
        p.getInventory().setContents(content);
    }

}
