package me.ItssLion.StaffMode;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;

public class ClickEvent implements Listener {

    private StaffMode main;

    ClickEvent(StaffMode main) {
        this.main = main;
    }

    @EventHandler
    public void onClick(InventoryClickEvent e) {
        Inventory destInvent = e.getInventory();
        int slotClicked = e.getRawSlot();
        if(!(slotClicked < destInvent.getSize())) {
            Player p = (Player) e.getWhoClicked();
            if (main.toggled.contains(p.getUniqueId())){ e.setCancelled(true);}
        }
    }
}
