package me.ItssLion.StaffMode;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class StaffModeCommand implements CommandExecutor {
    private StaffMode main;

    StaffModeCommand(StaffMode main) {
        this.main = main;
    }
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (cmd.getName().equalsIgnoreCase("staffmode")) {
            if (!(sender instanceof Player)) {
                sender.sendMessage(Utils.format("&c&lYou need to be a player to execute this command!"));
                return true;
            }
            Player p = (Player) sender;
            if (!p.hasPermission("overhaul.staff")) {
                p.sendMessage(Utils.format("&c&lYou need to have the permission 'overhaul.staff' to execute this command!"));
                return true;
            }
            if (main.toggled.contains(p.getUniqueId())) {
                main.toggled.remove(p.getUniqueId());
                InventoryManager.restoreInventory(p);
                p.sendMessage(Utils.format(main.prefix + "&aInventory restored!"));
            } else {
                main.toggled.add(p.getUniqueId());
                InventoryManager.saveInventory(p);
                p.getInventory().clear();
                p.sendMessage(Utils.format(main.prefix + "&aEntered staff mode!"));
                Utils.giveItems(p);
            }
        }


        return true;
    }
}
