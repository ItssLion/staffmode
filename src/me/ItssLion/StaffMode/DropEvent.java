package me.ItssLion.StaffMode;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerDropItemEvent;

public class DropEvent implements Listener {
    private StaffMode main;

    DropEvent(StaffMode main) {
        this.main = main;
    }

    @EventHandler
    public void onDrop(PlayerDropItemEvent e) {
        if (main.toggled.contains(e.getPlayer().getUniqueId())) {
            e.setCancelled(true);
        }
    }
}
